# This is a quick instruduction to file handling

# Implement the functions below

# filename = string (path to a file)
# returns the number of lines in the file
def linecount(filename):
    return 0

def main():
    split_test = [
            ["resources/f", 3],
            ["resources/g", 9]
            ]

    reverse_test = [
            ["abc","cba"],
            ["Dong","gnoD"],
            ["shuzhi","ihzuhs"]
            ]

    test(split_test, split)
    test(reverse_test, reverse)

def func_to_str(func, args):
    funcname = func.__name__
    if len(args) == 0:
        return "{}()".format(funcname)

    argstr = ",".join([str(arg) for arg in args])
    return "{}({})".format(funcname,argstr)

def test(tests, func):
    i = 0
    failed = False
    res = ""
    for test in tests:
        i += 1
        res += "Running test {}; {}: ".format(i, func_to_str(func,test[0:-1]))
        val = func(*test[0:-1])
        if val != test[-1]:
            res += "Got '{}', expected '{}'\n".format(val, test[-1])
            failed = True
        else:
            res += "Succeded\n"
    if failed:
        print(res)
        print "tests {} failed".format(func.__name__)
    else:
        print "All tests for {} succeded".format(func.__name__)

if __name__ == "__main__":
    main()
