# Implement the two functions below


# s = string, c = string
# parameter c can be assumed to be only one character long
# returns a list of strings where s has been split by the character c
# example 1: split("abc,def,ghi", ",") returns ["abc", "def", "ghi"]
# example 2: split("abacaeadaa", "a") returns ["","b","c","ed","",""]
# do not use the builtin split function
def split(s, c):
    return ""

# s = string
# returns the reversed version of s
# example: reverse("abc") return "cba"
def reverse(s):
    return ""

def main():
    split_test = [
            ["","",[""]],
            ["abc,def,ghi", ",",["abc","def","ghi"]],
            [":aa:", ":", ["","aa",""]],
            ["abacaeadaa", "a", ["","b","c","ed","",""]]
            ]

    reverse_test = [
            ["abc","cba"],
            ["Dong","gnoD"],
            ["shuzhi","ihzuhs"]
            ]

    test(split_test, split)
    test(reverse_test, reverse)

def func_to_str(func, args):
    funcname = func.__name__
    if len(args) == 0:
        return "{}()".format(funcname)

    argstr = ",".join([str(arg) for arg in args])
    return "{}({})".format(funcname,argstr)

def test(tests, func):
    i = 0
    failed = False
    res = ""
    for test in tests:
        i += 1
        res += "Running test {}; {}: ".format(i, func_to_str(func,test[0:-1]))
        val = func(*test[0:-1])
        if val != test[-1]:
            res += "Got '{}', expected '{}'\n".format(val, test[-1])
            failed = True
        else:
            res += "Succeded\n"
    if failed:
        print(res)
        print "tests {} failed".format(func.__name__)
    else:
        print "All tests for {} succeded".format(func.__name__)

if __name__ == "__main__":
    main()
