I will in this file try to describe everything needed to solve the first
exercise. Good luck!

NUMBERS

Numbers work for the most part as one can expect, with possibility to add,
subtract, multiply and divide.

One thing that is good to keep in mind is that you can change the base of the
numbers. The base of a number decides how many numbers that are allowed as well
as how much the number is worth. The basic formula for numbers is: 
value * base^position

Example:
given the base 10 (the base we're used to) the number 123 becomes
3*10^0 = 3*1   =   3
2*10^1 = 2*10  =  20
1*10^2 = 1*100 = 100+
result           123

The same as above can be done for other bases as well.

Example:
given the base 8 the number 123 becomes
3*8^0 = 3*1  =  3
2*8^1 = 2*8  = 16
1*8^2 = 1*64 = 64+
result:        83

Base 2, 8 and 16 (especially base 16) is quite normally used in computers and
therefore many languages include ways to write numbers in these bases easily.
A general rule of thumb is that the amounts of numbers allowed in a base is from
0 to (base-1). so in base 10 the numbers 0-9 is allowed in base 8 the numbers
0-7 is allowed and in base 2 only 0-1 is allowed. Base 16 is a little special
since each position in the number can have the value of 0-15. Therefore it's
common pratice that A=10, B=11 ... F=15. 

Example:
given the base 16 in number FF becomes
15*16^0 = 15*1  =  15
15*16^1 = 15*16 = 240+
result:           255       

As previously mentioned many programming languages have good support to write
numbers base 2, 8, 10 and 16. The following syntax is used in python (and many
other programming languages aswell).

To write a number in base 2 you have to prefix the number by '0b'.

Example:
0b11 = 3 (in base 10)

To write a number in base 8 you have to prefix the number by '0'.

Example:
051 = 41 (in base 10)

To write a number in base 16 you have to prefix the number by '0x'.

Example:
0xCA = 202 (in base 10)

LISTS

Lists are a thing in python that allows you to store multiple values. Lists are
indexed by position which means that you can easily access for example the first
or the second element in the list.

The syntax to create a list in python is with square brackets ([]).

Example:
l = []

The line above gives you an empty list with the variable name 'l'. There are a
few useful things you can do with lists, such as add values to it. To add a
value, use the "append" function on the list.

Example:
l = []
l.append(4) # list now contains one element
l.append(5) # list now contains two elements

To get a value from a list you also use the square brackets. One thing that is
important to remember is that the first element in the list is obtained by
index 0.

Example:
l = []
l.append(4) # list now contains one element
l.append(5) # list now contains two elements
l[0] # gives me back 4
l[1] # gives me back 5
l[1] # gives me back 5 again

Lists can also be initialized when created if you know at what values should be
inside it.

Example:
l = [4, 5]


LOOPS

There are two loops in python. 'for' loops and 'while' loops. The for loop is
usually more useful and convenient to use, therefore that's where we start. for
loops are great looping through lists (or other things such as strings (text)).
When the thing you are looping over doesn't have any more values, it ends and
python will continues to execute what ever is after the loop.

The syntax to create a for loop looks like this.

Example:
l = [4,5]
for i in l:
    print i

The above code would print 4 and then 5. The 'i' in the loop is a variable that
can only be used inside the loop. First time through the loop the variable 'i'
will have the first value in the list. Second time through the loop the variable
'i' will have the second value in the list etc.

For loops can also be used with many things, not only lists. It can for example
be used with strings (text) as well.

Example:
s = "Hello"
for c in s:
    print c

In this example c will be the characters in the string s. First time through the
loop c will have the value "H", second time through the loop c will have the
value "e" etc. when the string doesn't have any more characters, the loop will
end.

FUNCTIONS

Functions are a very important part of any programming language. It is basically
a piece of code that is given a name. They usually take a few parameters and
returns something when its finished.

The syntax for functions are the following

Example:
def add(a,b):
    return a+b

I now created a function called add that takes two parameters, a and b. The
return statement returns a value where the two parameters added together. I can
call this function using the following syntax.

Example:
val = add(1,2) # val = 3

After this line has been run I will have a variable called val which contains
the value 3 (1+2). A small fun fact is that this function would work not only
for numbers, but also for strings or lists.

Example:
s = add("Hello", " Shuzhi") # s = "Hello Shuzhi"
l = add([1,2], [3,4]) # l = [1,2,3,4]

I usually prefer on my first line in my function create a variable called res
(result) that I will return when the function returns.

Example:
def add(a,b):
    res = a+b
    return res

In this example its not necessary but it might be a good thing to keep in mind
when coding.
