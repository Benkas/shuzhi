# Implement the three functions below according to the documentation. Solve the
# problems without using builtin functions such as sum().  When the functions
# are implemented correctly the output of the program says that all tests are
# successful. The rest of the code is just testing code.
#
# You can read the the test code if you want but it contains some more advanced
# python features. It's not important to understand it at this point.
#
# These functions can be solved in just 1 or a few lines. They are meant to be a
# refresher and to get you comfortable with your coding environment and how to
# run python programs


# str1 and str2 = strings
# returns str1 and str2 concatenated
# ex str1 = "abc", str2 = "def" returns "abcdef"
def concat_str(str1,str2):
    return ""

# ints = list of integers
# returns the sum of all integers in the list
# ex ints = [1,2,5] returns 1 + 2 + 5 = 8
def sum_ints(ints):
    return 0

# ints = list of integer
# returns the cumulative sum of the list
# ex ints = [1,2,3,4] returns [1,1+2,1+2+3,1+2+3+4] = [1,3,6,10]
def cumulative_sum(ints):
    return []

def main():
    str_test = [
            ["","",""],
            ["a", "b","ab"],
            ["ab","cd","abcd"],
            ["Dong", "Shuzhi", "DongShuzhi"]
            ]

    int_test = [
            [[1,2],3],
            [[2,4],6],
            [[1,4],5]
            ]

    int_cumu_test = [
            [[],[]],
            [[1,2,5],[1,3,8]],
            [[2,4,3],[2,6,9]],
            [[1,4,10],[1,5,15]]
            ]


    test(str_test, concat_str)
    test(int_test, sum_ints)
    test(int_cumu_test, cumulative_sum)

def func_to_str(func, args):
    funcname = func.__name__
    if len(args) == 0:
        return "{}()".format(funcname)

    argstr = ",".join([str(arg) for arg in args])
    return "{}({})".format(funcname,argstr)

def test(tests, func):
    i = 0
    failed = False
    res = ""
    for test in tests:
        i += 1
        res += "Running test {}; {}: ".format(i, func_to_str(func,test[0:-1]))
        val = func(*test[0:-1])
        if val != test[-1]:
            res += "Got '{}', expected '{}'\n".format(val, test[-1])
            failed = True
        else:
            res += "Succeded\n"
    if failed:
        print(res)
        print "tests {} failed".format(func.__name__)
    else:
        print "All tests for {} succeded".format(func.__name__)

if __name__ == "__main__":
    main()
